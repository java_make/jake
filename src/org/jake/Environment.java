package org.jake;

import java.util.HashMap;
import java.util.Map;

public final class Environment {

	public final static HashMap<String, String> ENVIRONMENT_VARS = new HashMap<String, String>(System.getenv());

	static {
		ENVIRONMENT_VARS.put("JAKE_OS", Utility.GET_OS().toString());
	}

	public final static void UPDATE(Map<String, String> _environment) {
		_environment.clear();
		_environment.putAll(ENVIRONMENT_VARS);
	}
}
