package org.jake;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public final class Utility {

	public final static void ASSET_FILE(int _lineNumber, File _file, String _canonicalPath) {
		if (!_file.exists()) {
			Logger.CRASH(_lineNumber, "File '" + _canonicalPath + "' does not exist.");
		}
		if (!_file.isFile()) {
			Logger.CRASH(_lineNumber, "File '" + _canonicalPath + "' is not a file.");
		}
	}

	public final static void ASSET_FOLDER(int _lineNumber, File _file, String _canonicalPath) {
		if (!_file.exists()) {
			Logger.CRASH(_lineNumber, "Folder '" + _canonicalPath + "' does not exist.");
		}
		if (!_file.isDirectory()) {
			Logger.CRASH(_lineNumber, "Folder '" + _canonicalPath + "' is not a folder.");
		}
	}

	public final static List<File> LIST_FILES_RECURSIVE(File _folder) {
		if (!_folder.isDirectory()) {
			return (Arrays.asList(_folder));
		}

		LinkedList<File> files = new LinkedList<>();
		LinkedList<File> folderStack = new LinkedList<>();
		folderStack.add(_folder);
		while (!folderStack.isEmpty()) {
			for (File current : folderStack.pop().listFiles()) {
				if (current.isDirectory()) {
					folderStack.addLast(current);
				} else {
					files.add(current);
				}
			}
		}

		return (files);
	}

	public final static void DELETE_FILES_RECURSIVELY(int _lineNumber, File _parent) {
		String canonicalPath = Utility.GET_CANONICAL_PATH(_lineNumber, _parent);

		if (!canonicalPath.contains("/.jake/")) {
			Logger.CRASH(_lineNumber, "Tried to delete file outside of .jake folder (" + canonicalPath + ").");
			return;
		}

		File[] subFiles;
		if (_parent.isFile() || (subFiles = _parent.listFiles()).length == 0) {
			if (!_parent.delete()) {
				Logger.CRASH(_lineNumber, "Could not remove '" + canonicalPath + "'.");
				return;
			} else {
				Logger.LOG(_lineNumber, "Removed '" + canonicalPath + "'.", Logger.SEVERITY.DEBUG);
			}
			return;
		}

		for (File subFile : subFiles) {
			DELETE_FILES_RECURSIVELY(_lineNumber, subFile);
		}

		DELETE_FILES_RECURSIVELY(_lineNumber, _parent);
	}

	public final static String PATH_SANITIZE(String _path) {
		if (File.separator.equals("\\")) {
			_path = _path.replaceAll("\\\\", "/");
		}

		return (_path);
	}

	public final static String SPACES_ESCAPE(String _input) {
		return (_input.replaceAll(" ", "\\\\ "));
	}

	public final static String GET_RELATIVE_PATH(int _lineNumber, File _from, File _file) {
		try {

			String s = _file.getCanonicalPath();
			String from = _from.getCanonicalPath();
			int same = 0;
			while (from.length() > same && s.length() > same && s.charAt(same) == from.charAt(same)) {
				same++;
			}
			s = s.substring(same);
			from = from.substring(same);
			if (!from.isEmpty()) {
				int above = from.split(File.separator).length;
				for (int i = 0; i < above; i++) {
					s = "../" + s;
				}
			}

			s = PATH_SANITIZE(s);
			return (s);

		} catch (IOException ex) {
			Logger.CRASH(_lineNumber, ex);
			return (null);
		}
	}

	public final static String GET_CANONICAL_PATH(int _lineNumber, File _file) {
		try {
			return (Utility.PATH_SANITIZE(_file.getCanonicalPath()));
		} catch (IOException ex) {
			Logger.CRASH(_lineNumber, ex);
		}
		return (null);
	}

	public final static void COPY_OR_REPLACE_FILE(int _lineNumber, File _sourceFile, File _destFile) {
		try {
			Files.copy(_sourceFile.toPath(), _destFile.toPath(), StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
		} catch (IOException ex) {
			Logger.CRASH(_lineNumber, ex);
			return;
		}
	}

	public final static String FOLDER_HASH(int _lineNumber, File _file) {
		try {

			StringBuilder folderHash = new StringBuilder();
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			byte[] bytes = md.digest(_file.getParent().getBytes());
			for (byte b : bytes) {
				String s = Integer.toHexString(b & 0xFF);
				if (b < 0x10) {
					folderHash.append('0');
				}
				folderHash.append(s);
			}

			return (folderHash.toString());

		} catch (NoSuchAlgorithmException ex) {
			Logger.CRASH(_lineNumber, ex);
			return (null);
		}
	}

	public final static File PARSE_USER_PATH(String _userPath) {
		if (Paths.get(_userPath).isAbsolute()) {
			return (new File(_userPath));
		}

		return (new File(Main.WORKING_DIRECTORY, _userPath));
	}

	public static enum OS {
		UNDEFINED,
		LINUX,
		WINDOWS,
		OSX
	}

	public final static OS GET_OS() {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("lin")) {
			return (OS.LINUX);
		} else if (os.contains("win")) {
			return (OS.WINDOWS);
		} else if (os.contains("mac") || os.contains("darwin")) {
			return (OS.OSX);
		}
		return (OS.UNDEFINED);
	}

}
