package org.jake;

import java.io.PrintWriter;
import java.io.StringWriter;

public final class Logger {

	public enum SEVERITY {
		DEBUG,
		INFO,
		WARN,
		ERROR;

		@Override
		public String toString() {
			return ("[" + super.toString() + "]");
		}
	}

	public static SEVERITY MIN_SEVERITY = SEVERITY.INFO;

	public final static void LOG(int _line, String _message, SEVERITY _severity) {
		if (_severity.compareTo(MIN_SEVERITY) < 0) {
			return;
		}

		if (_line == 0) {
			String format = "JAKE %-7S       - %s\n";
			System.out.printf(format, _severity.toString(), _message);
			if (_severity == SEVERITY.ERROR) {
				System.err.printf(format, _severity.toString(), _message);
			}
		} else {
			String format = "JAKE %-7S L#%03d - %s\n";
			System.out.printf(format, _severity.toString(), _line, _message);
			if (_severity == SEVERITY.ERROR) {
				System.err.printf(format, _severity.toString(), _line, _message);
			}
		}
	}

	public final static void CRASH(int _line, String _message) {
		LOG(_line, _message, SEVERITY.ERROR);
		System.exit(1);
	}

	public final static void CRASH(int _line, Throwable _message) {
		StringWriter sw = new StringWriter();
		_message.printStackTrace(new PrintWriter(sw));
		CRASH(_line, sw.toString());
	}

}
