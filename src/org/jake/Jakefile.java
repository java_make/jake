package org.jake;

import java.util.LinkedList;
import org.jake.command.Command;
import org.jake.command.Command_Branch;
import org.jake.command.Command_EndBranch;

public final class Jakefile {

	private final LinkedList<Command> commands_;
	private final LinkedList<Boolean> branchStack_ = new LinkedList<>();

	protected Jakefile(LinkedList<Command> _commands) {
		commands_ = _commands;
		branchStack_.addFirst(true);
	}

	public final void run() {
		for (Command command : commands_) {
			try {
				if (command.getClass() == Command_EndBranch.class) {
					branchStack_.pop();
					if (branchStack_.isEmpty()) {
						Logger.CRASH(command.LineNumber, "There is no branch to end.");
					}
				}

				if (!branchStack_.peek()) {
					continue;
				}

				Logger.LOG(command.LineNumber, command.toString(), Logger.SEVERITY.DEBUG);
				command.prepareAndExecute();

				if (Command_Branch.class.isAssignableFrom(command.getClass())) {
					branchStack_.addFirst(((Command_Branch) command).ConditionMet);
				}
			} catch (Exception ex) {
				Logger.CRASH(command.LineNumber, ex);
				return;
			}
		}
	}

}
