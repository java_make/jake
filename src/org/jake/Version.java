package org.jake;

public final class Version {

	public final static String VERSION_MAJOR = "%VERSION_MAJOR%";
	public final static String VERSION_MINOR = "%VERSION_MINOR%";
	public final static String VERSION_PATCH = "%VERSION_PATCH%";
	public final static String VERSION_GIT_COMMIT = "%GIT_COMMIT%";

	public final static String VERSION() {
		return (VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_PATCH);
	}

	public final static String VERSION_AND_GIT() {
		String gitCommit = VERSION_GIT_COMMIT;
		if (gitCommit.length() > 8) {
			gitCommit = VERSION_GIT_COMMIT.substring(0, 8);
		}
		return ("Version " + VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_PATCH + " (" + gitCommit + ")");
	}

}
