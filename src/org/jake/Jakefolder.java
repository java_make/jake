package org.jake;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import publicdomain.JSON;

public final class Jakefolder {

	public final static File BASE_FOLDER = FOLDER_CREATE("");
	public final static File SOURCE_FOLDER = FOLDER_CREATE("source");
	public final static File SOURCE_TREE_FOLDER = FOLDER_CREATE("source_tree");
	public final static File BUILD_FOLDER = FOLDER_CREATE("build");
	public final static File LIBRARY_FOLDER = FOLDER_CREATE("library");
	public final static File DIST_FOLDER = FOLDER_CREATE("dist");

	private final static File TRACKED_SOURCE_FILES_FILE = new File(BASE_FOLDER, "trackedSourceFiles.json");
	private final static JSON TRACKED_SOURCE_FILES;
	private static boolean TRACKED_SOURCE_FILES_CHANGED = false;

	private final static File TRACKED_LIBRARY_FILES_FILE = new File(BASE_FOLDER, "trackedLibraryFiles.json");
	private final static JSON TRACKED_LIBRARY_FILES;
	private static boolean TRACKED_LIBRARY_FILES_CHANGED = false;

	private final static File TRACKED_BUILD_FILES_FILE = new File(BASE_FOLDER, "trackedBuildFiles.json");
	private final static JSON TRACKED_BUILD_FILES;
	private static boolean TRACKED_BUILD_FILES_CHANGED = false;

	private final static JSON[] TRACKED_FILES;

	public final static File MANIFEST_FILE = new File(BASE_FOLDER, "MANIFEST.MF");

	private static JSON JSON_LOAD(File _file) {
		JSON f;

		try {
			if (!_file.exists()) {
				f = new JSON(JSON.JSONVALUE_TYPE.JSON_OBJECT);
				f.json_writeToFile(_file, 32, true);
			} else if (!_file.isFile()) {
				Logger.CRASH(0, "'" + Utility.GET_CANONICAL_PATH(0, _file) + "' is not a file.");
				return (null);
			} else {
				f = new JSON(_file);
			}
		} catch (IOException | IllegalStateException ex) {
			Logger.CRASH(0, ex);
			return (null);
		}

		return (f);
	}

	private static void JSON_WRITE(JSON _json, File _file) {
		try {
			_json.json_writeToFile(_file, 128, true);
		} catch (IOException ex) {
			Logger.CRASH(0, ex);
			return;
		}
	}

	static {
		TRACKED_SOURCE_FILES = JSON_LOAD(TRACKED_SOURCE_FILES_FILE);
		TRACKED_LIBRARY_FILES = JSON_LOAD(TRACKED_LIBRARY_FILES_FILE);
		TRACKED_BUILD_FILES = JSON_LOAD(TRACKED_BUILD_FILES_FILE);

		TRACKED_FILES = new JSON[]{TRACKED_SOURCE_FILES, TRACKED_BUILD_FILES, TRACKED_LIBRARY_FILES};

		CLEAN_MANIFEST(0);
	}

	public final static void CLEAN_MANIFEST(int _lineNumber) {
		if (MANIFEST_FILE.exists()) {
			MANIFEST_FILE.delete();
		}

		try {
			if (!MANIFEST_FILE.createNewFile()) {
				Logger.CRASH(_lineNumber, "Could not create MANIFEST.MF file.");
			}
		} catch (IOException ex) {
			Logger.CRASH(_lineNumber, ex);
		}
	}

	public final static File FOLDER_CREATE(String _foldername) {
		File file = new File(Main.WORKING_DIRECTORY, ".jake/" + _foldername);

		String canonicalPath = Utility.GET_CANONICAL_PATH(0, file);

		if (!file.exists()) {
			Logger.LOG(0, "Creating folder '" + Utility.PATH_SANITIZE(file.getPath()) + "'.", Logger.SEVERITY.INFO);
			try {
				if (!file.mkdirs()) {
					Logger.CRASH(0, "Creating folder '" + canonicalPath + "' failed.");
				}
			} catch (Exception ex) {
				Logger.CRASH(0, ex);
			}
		} else if (file.isDirectory()) {
			Logger.LOG(0, "'" + canonicalPath + "' folder does already exist.", Logger.SEVERITY.DEBUG);
		} else {
			Logger.CRASH(0, "'" + canonicalPath + "' is not a folder.");
		}

		return (file);
	}

	private static boolean FILE_CHANGED(JSON _tracker, String _canonicalPath) {
		if (!_tracker.json_containsKey(_canonicalPath)) {
			return (true);
		}

		JSON metadata = _tracker.json_getObject(_canonicalPath);
		File sourcefile = new File(_canonicalPath);

		if (metadata.json_containsKey("destination")) {
			if (!new File(metadata.json_getString("destination")).exists()) {
				return (true);
			}
		}

		boolean noinfo = true;

		if (metadata.json_containsKey("lastmodified")) {
			noinfo = false;
			long lastmodified = sourcefile.lastModified();
			if (lastmodified != metadata.json_getLong("lastmodified")) {
				return (true);
			}
		}

		if (metadata.json_containsKey("filesize")) {
			noinfo = false;
			long filesize = sourcefile.length();
			try {
				if (filesize != metadata.json_getInt("filesize")) {
					return (true);
				}
			} catch (ClassCastException ex) {
				if (filesize != metadata.json_getLong("filesize")) {
					return (true);
				}
			}
		}

		return (noinfo);
	}

	private static void FILE_ADD_HASH(int _lineNumber, String _canonicalPath, JSON _container, File _folder) {
		File sourceFile = new File(_canonicalPath);

		String folderHash = Utility.FOLDER_HASH(_lineNumber, sourceFile);
		File jakeFolderName = new File(_folder.getName(), folderHash);
		File jakeFolder = Jakefolder.FOLDER_CREATE(jakeFolderName.getPath());

		File destFile = new File(jakeFolder, sourceFile.getName());
		Utility.COPY_OR_REPLACE_FILE(_lineNumber, sourceFile, destFile);

		_container.json_add(_canonicalPath,
				new JSON(JSON.JSONVALUE_TYPE.JSON_OBJECT)
						.json_add("lastmodified", sourceFile.lastModified())
						.json_add("filesize", sourceFile.length())
						.json_add("destination", Utility.GET_CANONICAL_PATH(_lineNumber, destFile))
		);
	}

	private static void FILE_ADD(int _lineNumber, File _sourceFile, String _relativePath, JSON _container, File _folder) {
		File jakeFolderName = new File(_folder.getName(), _relativePath);
		File jakeFolder = Jakefolder.FOLDER_CREATE(jakeFolderName.getPath());

		File destFile = new File(jakeFolder, _sourceFile.getName());
		Utility.COPY_OR_REPLACE_FILE(_lineNumber, _sourceFile, destFile);

		_container.json_add(Utility.GET_CANONICAL_PATH(_lineNumber, _sourceFile),
				new JSON(JSON.JSONVALUE_TYPE.JSON_OBJECT)
						.json_add("lastmodified", _sourceFile.lastModified())
						.json_add("filesize", _sourceFile.length())
						.json_add("destination", Utility.GET_CANONICAL_PATH(_lineNumber, destFile))
		);
	}

	public final static boolean SOURCE_FILE_CHANGED(String _canonicalPath) {
		return (FILE_CHANGED(TRACKED_SOURCE_FILES, _canonicalPath));
	}

	public final static void SOURCE_FILE_ADD(int _lineNumber, String _canonicalPath) {
		FILE_ADD_HASH(_lineNumber, _canonicalPath, TRACKED_SOURCE_FILES, SOURCE_FOLDER);
		TRACKED_SOURCE_FILES_CHANGED = true;
	}

	public final static void SOURCE_TREE_FILE_ADD(int _lineNumber, File _sourceFile, String _relativePath) {
		FILE_ADD(_lineNumber, _sourceFile, _relativePath, TRACKED_SOURCE_FILES, SOURCE_TREE_FOLDER);
		TRACKED_SOURCE_FILES_CHANGED = true;
	}

	public final static boolean LIBRARY_FILE_CHANGED(String _canonicalPath) {
		return (FILE_CHANGED(TRACKED_LIBRARY_FILES, _canonicalPath));
	}

	public final static void LIBRARY_FILE_ADD(int _lineNumber, String _canonicalPath) {
		FILE_ADD_HASH(_lineNumber, _canonicalPath, TRACKED_LIBRARY_FILES, LIBRARY_FOLDER);
		TRACKED_LIBRARY_FILES_CHANGED = true;
	}

	public final static boolean BUILD_FILE_CHANGED(String _canonicalPath) {
		return (FILE_CHANGED(TRACKED_BUILD_FILES, _canonicalPath));
	}

	public final static void UPDATE_TRACKED_FILE(int _lineNumber, String _canonicalPath) {
		for (JSON container : TRACKED_FILES) {
			for (Map.Entry<String, Object> entry : container.iteratorObject()) {
				JSON json = ((JSON) entry.getValue());
				if (json.json_getString("destination").equals(_canonicalPath)) {
					File f = new File(_canonicalPath);
					json.json_add("lastmodified", f.lastModified())
							.json_add("filesize", f.length());
					Logger.LOG(_lineNumber, "Updated meta information of " + entry.getKey() + ".", Logger.SEVERITY.DEBUG);
					return;
				}
			}
		}
	}

	public final static void BUILD_FILE_ADDED(int _lineNumber, File _file, String _canonicalPath, boolean _sourceTree) {
		JSON info = new JSON(JSON.JSONVALUE_TYPE.JSON_OBJECT)
				.json_add("lastmodified", _file.lastModified())
				.json_add("filesize", _file.length());

		if (_sourceTree) {
			String relativePath = Utility.GET_RELATIVE_PATH(_lineNumber, Jakefolder.SOURCE_TREE_FOLDER, _file);

			File destFile = new File(Jakefolder.BUILD_FOLDER, relativePath);
			String filename = _file.getName();
			if (filename.endsWith(".java")) {
				destFile = new File(destFile.getParentFile(), filename.substring(0, filename.length() - 5) + ".class");
			}

			info.json_add("destination", Utility.GET_CANONICAL_PATH(_lineNumber, destFile));
		}

		TRACKED_BUILD_FILES.json_add(_canonicalPath, info);
		TRACKED_BUILD_FILES_CHANGED = true;
	}

	public static void PREPARE_REBUILD_TREE_FILE(int _lineNumber, String _canonicalPath) {
		if (!TRACKED_BUILD_FILES.json_containsKey(_canonicalPath)) {
			return;
		}

		JSON info = TRACKED_BUILD_FILES.json_getObject(_canonicalPath);

		if (!info.json_containsKey("destination")) {
			return;
		}

		File f = new File(info.json_getString("destination"));
		if (!f.exists()) {
			return;
		}

		Logger.LOG(_lineNumber, "Deleting file '" + Utility.PATH_SANITIZE(f.getPath()) + "' before compiling.", Logger.SEVERITY.DEBUG);

		if (!f.delete()) {
			Logger.LOG(_lineNumber, "File '" + Utility.PATH_SANITIZE(f.getPath()) + "' could not be deleted.", Logger.SEVERITY.WARN);
		}
	}

	public final static void CLEAN(int _lineNumber, String _folder) {
		File[] folders;
		switch (_folder) {
			case "SOURCES":
				folders = new File[]{
					Jakefolder.SOURCE_FOLDER,
					Jakefolder.SOURCE_TREE_FOLDER
				};
				TRACKED_SOURCE_FILES.json_init();
				TRACKED_SOURCE_FILES_CHANGED = true;
				break;
			case "LIBRARIES":
				folders = new File[]{
					Jakefolder.LIBRARY_FOLDER
				};
				TRACKED_LIBRARY_FILES.json_init();
				TRACKED_LIBRARY_FILES_CHANGED = true;
				break;
			case "BUILD":
				folders = new File[]{
					Jakefolder.BUILD_FOLDER
				};
				TRACKED_BUILD_FILES.json_init();
				TRACKED_BUILD_FILES_CHANGED = true;
				break;
			case "DIST":
				folders = new File[]{
					Jakefolder.DIST_FOLDER
				};
				break;
			case "MANIFEST":
				Logger.LOG(_lineNumber, "Cleaning MANIFEST file", Logger.SEVERITY.INFO);
				Jakefolder.CLEAN_MANIFEST(_lineNumber);
				return;
			default:
				Logger.CRASH(_lineNumber, "Invalid folder '" + _folder + "'.");
				return;
		}

		Logger.LOG(_lineNumber, "Cleaning " + _folder + ".", Logger.SEVERITY.INFO);

		for (File f : folders) {
			for (File subFile : f.listFiles()) {
				Utility.DELETE_FILES_RECURSIVELY(_lineNumber, subFile);
			}
		}

		SAVE_TRACKED_FILES();
	}

	public final static void SAVE_TRACKED_FILES() {
		if (TRACKED_SOURCE_FILES_CHANGED) {
			JSON_WRITE(TRACKED_SOURCE_FILES, TRACKED_SOURCE_FILES_FILE);
		}

		if (TRACKED_LIBRARY_FILES_CHANGED) {
			JSON_WRITE(TRACKED_LIBRARY_FILES, TRACKED_LIBRARY_FILES_FILE);
		}

		if (TRACKED_BUILD_FILES_CHANGED) {
			JSON_WRITE(TRACKED_BUILD_FILES, TRACKED_BUILD_FILES_FILE);
		}
	}

	public final static String CLASSPATH_GET(String _projectBuildPath) {
		StringBuilder classpath = new StringBuilder();

		classpath.append(_projectBuildPath);

		for (File f : LIBRARY_FOLDER.listFiles()) {
			if (!f.isDirectory()) {
				continue;
			}

			if (classpath.length() != 0) {
				classpath.append(File.pathSeparatorChar);
			}

			classpath.append(Utility.PATH_SANITIZE(f.getPath()));
			classpath.append('/');
			classpath.append('*');
		}

		return (classpath.toString());
	}

}
