package org.jake;

import java.io.File;
import java.io.IOException;
import org.jake.command.Commands;

public class Main {

	public static String WORKING_DIRECTORY = null;

	public static void main(String[] _args) throws IOException {

		File workingDirectory = new File(".");

		boolean sections = false;
		for (int i = 0; i < _args.length; i++) {
			String arg = _args[i];
			if (arg.startsWith("-")) {
				if (sections) {
					Logger.CRASH(0, "Invalid section arguments.");
					return;
				}

				switch (arg.substring(1)) {
					case "-loglevel":
					case "l":
						if ((i + 1) >= _args.length) {
							Logger.CRASH(0, "Missing argument.");
							return;
						}
						try {
							Logger.MIN_SEVERITY = Logger.SEVERITY.valueOf(_args[++i]);
							Logger.LOG(0, "Setting log level to " + Logger.MIN_SEVERITY.toString() + ".", Logger.SEVERITY.INFO);
						} catch (IllegalArgumentException ex) {
							Logger.CRASH(0, "Invalid log level.");
							return;
						}
						break;
					case "-workdir":
					case "d": {
						if ((i + 1) >= _args.length) {
							Logger.CRASH(0, "Missing argument.");
							return;
						}
						workingDirectory = new File(_args[++i]);
					}
					break;
					case "-version":
					case "v":
						Logger.LOG(0, Version.VERSION_AND_GIT(), Logger.SEVERITY.INFO);
						System.exit(0);
						break;
					case "-commands":
					case "c": {
						StringBuilder help = new StringBuilder();
						help.append("\n\nJake commands:\n\n");
						help.append(Commands.HELP());
						Logger.LOG(0, help.toString(), Logger.SEVERITY.INFO);
						System.exit(0);
					}
					break;
					case "-help":
					case "h": {
						StringBuilder help = new StringBuilder();
						help.append("\nUsage: jake.jar [OPTION...] [SECTION...]\n\n");
						help.append("Command line options:\n");
						help.append("-h --help                            \tdisplay this help\n");
						help.append("-c --commands                        \tdisplay all Jakefile commands\n");
						help.append("-l --loglevel (DEBUG|INFO|WARN|ERROR)\tset minimum log level\n");
						help.append("-v --version                         \tdisplay the version of jake\n");
						help.append("-d --workdir                         \tset the working directory to something else\n");
						help.append("\nSections:\n");
						help.append("Add any number of sections after the command line options. Sections may not start with '-'.\n");
						Logger.LOG(0, help.toString(), Logger.SEVERITY.INFO);
						System.exit(0);
					}
					break;
					default:
						Logger.CRASH(0, "Invalid command line argument '" + arg + "'.");
						return;
				}
			} else {
				sections = true;
				Logger.LOG(0, "Section enabled '" + arg + "'", Logger.SEVERITY.DEBUG);
				Environment.ENVIRONMENT_VARS.put("JAKE_SECTION_" + arg, "");
			}
		}

		Logger.LOG(0, "v" + Version.VERSION(), Logger.SEVERITY.DEBUG);

		if (!workingDirectory.exists() || !workingDirectory.isDirectory()) {
			Logger.CRASH(0, "Specified working directory '" + Utility.PATH_SANITIZE(workingDirectory.getPath()) + "' does not exist.");
			return;
		}

		WORKING_DIRECTORY = Utility.GET_CANONICAL_PATH(0, workingDirectory);
		Logger.LOG(0, "Working directory '" + Utility.PATH_SANITIZE(WORKING_DIRECTORY) + "'.", Logger.SEVERITY.DEBUG);

		File file = null;
		for (File f : workingDirectory.listFiles()) {
			if (f.getName().equalsIgnoreCase("jakefile")) {
				file = f;
				break;
			}
		}
		if (file == null) {
			Logger.CRASH(0, "Jakefile not found.");
			return;
		}

		Jakefile jakefile = JakefileParser.PARSE_FILE(file);
		jakefile.run();

		Jakefolder.SAVE_TRACKED_FILES();

		Logger.LOG(0, "Done.", Logger.SEVERITY.INFO);
	}

}
