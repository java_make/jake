package org.jake.command;

import java.io.File;
import org.jake.Jakefolder;
import org.jake.Logger;
import org.jake.Utility;

public final class Command_Jar extends Command {

	public Command_Jar(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		File output = new File(Jakefolder.DIST_FOLDER, commandArgs_[0]);
		String canonicalPath = Utility.GET_CANONICAL_PATH(LineNumber, output);
		String log = "Creating archive '" + Utility.PATH_SANITIZE(output.getPath()) + "'";

		if (commandArgs_.length == 2) {
			new Command_Manifest(LineNumber, Utility.SPACES_ESCAPE("Main-Class: " + commandArgs_[1])).execute();

			log += " with main class '" + commandArgs_[1] + "'";
		}

		Logger.LOG(LineNumber, log + ".", Logger.SEVERITY.INFO);

		new Command_Process(LineNumber, new String[]{
			"jar",
			"cfm",
			canonicalPath,
			Utility.GET_CANONICAL_PATH(LineNumber, Jakefolder.MANIFEST_FILE),
			"-C",
			Utility.GET_CANONICAL_PATH(LineNumber, Jakefolder.BUILD_FOLDER),
			"."
		}).execute();
	}

}
