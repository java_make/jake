package org.jake.command;

import org.jake.Environment;
import org.jake.Logger;

public final class Command_SetEnv extends Command {

	public Command_SetEnv(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		Logger.LOG(LineNumber, "Setting environment variable '" + commandArgs_[0] + "' to '" + commandArgs_[1] + "'.", Logger.SEVERITY.DEBUG);
		Environment.ENVIRONMENT_VARS.put(commandArgs_[0], commandArgs_[1]);
	}

}
