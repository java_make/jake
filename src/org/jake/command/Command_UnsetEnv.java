package org.jake.command;

import org.jake.Environment;
import org.jake.Logger;

public final class Command_UnsetEnv extends Command {

	public Command_UnsetEnv(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		Logger.LOG(LineNumber, "Removing environment variable '" + commandArgs_[0] + "'.", Logger.SEVERITY.DEBUG);
		Environment.ENVIRONMENT_VARS.remove(commandArgs_[0]);
	}

}
