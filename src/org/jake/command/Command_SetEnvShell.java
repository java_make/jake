package org.jake.command;

import org.jake.Logger;
import org.jake.Utility;

public class Command_SetEnvShell extends Command {

	private final Command_Shell cShell_;

	public Command_SetEnvShell(int _lineNumber, String _commandArgs) {
		super(_lineNumber);
		commandArgs_ = _commandArgs.split("(?<!\\\\) ", 2);
		cShell_ = new Command_Shell(LineNumber, commandArgs_.length > 1 ? commandArgs_[1] : "", true);
	}

	@Override
	public void execute() {
		Logger.LOG(LineNumber, "Setting '" + commandArgs_[0] + "' via shell execution.", Logger.SEVERITY.INFO);
		cShell_.execute();
		String output = Utility.SPACES_ESCAPE(cShell_.Output.trim());
		new Command_SetEnv(LineNumber, commandArgs_[0] + " " + output).execute();
	}

}
