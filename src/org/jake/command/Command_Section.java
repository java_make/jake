package org.jake.command;

public final class Command_Section extends Command_IsEnv {

	public Command_Section(int _lineNumber, String _commandArgs) {
		super(_lineNumber, "JAKE_SECTION_" + _commandArgs);
	}

}
