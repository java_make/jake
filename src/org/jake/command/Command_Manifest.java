package org.jake.command;

import java.io.FileWriter;
import java.io.IOException;
import org.jake.Jakefolder;
import org.jake.Logger;

public final class Command_Manifest extends Command {

	public Command_Manifest(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		if (commandArgs_[0].length() > 70) {
			StringBuilder manifest = new StringBuilder(commandArgs_[0]);
			for (int i = 70; i < manifest.length(); i += 70) {
				manifest.insert(i, '\n');
				manifest.insert(i + 1, ' ');
				i++;
			}
			commandArgs_[0] = manifest.toString();
		}

		try (FileWriter fr = new FileWriter(Jakefolder.MANIFEST_FILE, true)) {
			fr.write(commandArgs_[0] + "\n");
			fr.flush();
		} catch (IOException ex) {
			Logger.CRASH(LineNumber, ex);
			return;
		}
	}
}
