package org.jake.command;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.jake.Jakefolder;
import org.jake.Logger;
import org.jake.Utility;

public class Command_Replace extends Command {

	public Command_Replace(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		if (commandArgs_[1].contains(commandArgs_[0])) {
			Logger.CRASH(LineNumber, "Replace string may not contain search string.");
			return;
		}

		Logger.LOG(LineNumber, "Replacing '" + commandArgs_[0] + "' with '" + commandArgs_[1] + "'.", Logger.SEVERITY.INFO);
		int replacedFiles = 0;

		replacedFiles += replace(Utility.LIST_FILES_RECURSIVE(Jakefolder.SOURCE_FOLDER));
		replacedFiles += replace(Utility.LIST_FILES_RECURSIVE(Jakefolder.SOURCE_TREE_FOLDER));

		Logger.LOG(LineNumber, "Replaced strings in " + replacedFiles + " files.", Logger.SEVERITY.INFO);
	}

	private int replace(List<File> _files) {

		int replacedFiles = 0;
		StringBuilder stringBuilder = new StringBuilder();
		for (File f : _files) {
			try (BufferedReader br = new BufferedReader(new FileReader(f))) {
				String line;
				while ((line = br.readLine()) != null) {
					stringBuilder.append(line).append("\n");
				}
			} catch (IOException ex) {
				Logger.CRASH(LineNumber, ex);
				return (0);
			}

			int index;
			boolean replaced = false;
			while ((index = stringBuilder.indexOf(commandArgs_[0])) != -1) {
				stringBuilder.replace(index, index + commandArgs_[0].length(), commandArgs_[1]);
				replaced = true;
			}

			if (replaced) {
				try {
					try (BufferedWriter bw = new BufferedWriter(new FileWriter(f))) {
						bw.write(stringBuilder.toString());
						bw.flush();
					}
				} catch (IOException ex) {
					Logger.CRASH(LineNumber, ex);
					return (0);
				}

				Jakefolder.UPDATE_TRACKED_FILE(LineNumber, Utility.GET_CANONICAL_PATH(LineNumber, f));

				replacedFiles++;
			}

			stringBuilder.delete(0, stringBuilder.length());
		}

		return (replacedFiles);
	}
}
