package org.jake.command;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.function.Predicate;

public final class Commands {

	public final static HashMap<String, Class<? extends Command>> STRING_TO_COMMAND = new HashMap<>();
	public final static HashMap<String, String> STRING_TO_DESCRIPTION = new HashMap<>();
	public final static HashMap<Class<? extends Command>, Argument[]> COMMAND_ARGUMENTS = new HashMap<>();

	public final static class Argument {

		public final String Name;
		public final boolean Required;
		public final boolean ResolveEnvironment;
		public final String Description;
		public final Predicate<String> Filter;

		Argument(String _name, boolean _required, boolean _resolveEnvironment, String _description, Predicate<String> _filter) {
			Name = _name;
			Required = _required;
			ResolveEnvironment = _resolveEnvironment;
			Description = _description;
			Filter = _filter;
		}

	}

	private static void COMMAND_ADD(String _command, Class<? extends Command> _class, String _description, Argument... _args) {
		if (STRING_TO_COMMAND.containsKey(_command)) {
			throw new AssertionError(STRING_TO_COMMAND.containsKey(_command));
		}
		if (COMMAND_ARGUMENTS.containsKey(_class)) {
			throw new AssertionError(COMMAND_ARGUMENTS.containsKey(_class));
		}

		STRING_TO_COMMAND.put(_command, _class);
		STRING_TO_DESCRIPTION.put(_command, _description);

		boolean required = true;
		for (Argument a : _args) {
			if (a.Required && !required) {
				throw new AssertionError(!a.Required || required);
			}
			required = a.Required;
		}

		COMMAND_ARGUMENTS.put(_class, _args);
	}

	private final static Predicate<String> NO_FILTER = (String _t) -> {
		return (true);
	};

	private final static Predicate<String> NO_BACKSLASH = (String _t) -> {
		return (!_t.contains("\\"));
	};

	private final static Predicate<String> JAVA_FILE = (String _t) -> {
		return (_t.endsWith(".java") && NO_BACKSLASH.test(_t));
	};

	private final static Predicate<String> NO_NEW_LINE = (String _t) -> {
		return (!_t.contains("\n") && !_t.contains("\r"));
	};

	private final static Predicate<String> NO_WHITE_SPACE = (String _t) -> {
		return (NO_NEW_LINE.test(_t) && !_t.contains(" ") && !_t.contains("\t"));
	};

	private final static Predicate<String> FILE_NAME = (String _t) -> {
		return (!_t.contains("/") && !_t.contains("\0") && NO_NEW_LINE.test(_t) && NO_BACKSLASH.test(_t));
	};

	private final static Predicate<String> CLEANABLE = (String _t) -> {
		switch (_t) {
			case "SOURCES":
			case "LIBRARIES":
			case "BUILD":
			case "DIST":
			case "MANIFEST":
				return (true);
			default:
				return (false);
		}
	};

	static {
		COMMAND_ADD("$", Command_Shell.class, "Executes shell commands.",
				new Argument(
						"shell",
						true,
						false,
						"The shell command to execute. Execution waits for the shell command to finish. A non-zero exit code causes the Jake process to terminate.",
						NO_FILTER
				)
		);
		COMMAND_ADD("SOURCE_FILE", Command_SourceFile.class, "Adds a single source file.",
				new Argument(
						"file",
						true,
						true,
						"The source file to add. This will track an unorganized .java file for compilation. No resource files can be added using SOURCE_FILE. The argument has to be a .java file.",
						JAVA_FILE
				)
		);
		COMMAND_ADD("SOURCE_FOLDER", Command_SourceFolder.class, "Adds a source folder.",
				new Argument(
						"folder",
						true,
						true,
						"The source folder to add. This will track all unorganized .java files in the folder recursively for compilation. No resource files can be added using SOURCE_FOLDER.",
						NO_BACKSLASH
				)
		);
		COMMAND_ADD("SOURCE_TREE", Command_SourceTree.class, "Adds a folder and subfolders as source tree.",
				new Argument(
						"tree",
						true,
						true,
						"The source tree to add. This will all sub-folders and files recursively for compilation. Resource files, aka non .java files, will also be included.",
						NO_BACKSLASH
				)
		);
		COMMAND_ADD("LIBRARY_FILE", Command_LibraryFile.class, "Adds a single file as library.",
				new Argument(
						"file",
						true,
						true,
						"Adds a .jar file to the list of compile time libraries. Non .jar files are ignored.",
						NO_BACKSLASH
				)
		);
		COMMAND_ADD("LIBRARY_FOLDER", Command_LibraryFolder.class, "Adds a library folder.",
				new Argument(
						"folder",
						true,
						true,
						"Adds all .jar files in this folder recursively to the list of compile time libraries. Non .jar files are ignored.",
						NO_BACKSLASH
				)
		);
		COMMAND_ADD("COMPILE", Command_Compile.class, "Compiles all changed tracked files.");
		COMMAND_ADD("MANIFEST", Command_Manifest.class, "Adds the given string to the MANIFEST.MF file",
				new Argument(
						"content",
						true,
						true,
						"Adds the given string to the MANIFEST.MF file, which is later included in the .jar file. The argument may not contain newline characters.",
						NO_NEW_LINE
				)
		);
		COMMAND_ADD("DISTRIBUTE_LIBRARIES", Command_DistributeLibraries.class, "Distribute the tracked libraries to the end user.");
		COMMAND_ADD("JAR", Command_Jar.class, "Packs all compiled files and the MANIFEST.MF file into a single .jar file.",
				new Argument(
						"filename",
						true,
						true,
						"The file name of the resulting .jar. May not contain / \\ \\n or \\r.",
						FILE_NAME
				),
				new Argument(
						"main-class",
						false,
						true,
						"The class to be automatically started when the .jar file is executed. The main class is written to the MANIFEST.MF. Library .jar files do not need to provide this. The argument may not contain newline characters.",
						NO_NEW_LINE
				)
		);
		COMMAND_ADD("CLEAN", Command_Clean.class, "Cleans the given internal Jake folder or file.",
				new Argument(
						"folder",
						true,
						false,
						"The folder or file to clean. Acceptable values are 'SOURCES', 'LIBRARIES', 'BUILD', 'DIST' and 'MANIFEST'.",
						CLEANABLE
				)
		);
		COMMAND_ADD("SET_ENV", Command_SetEnv.class, "Sets a environment variable with the given key to the given value.",
				new Argument(
						"key",
						true,
						false,
						"The environment variable to set.",
						NO_WHITE_SPACE
				),
				new Argument(
						"value",
						true,
						true,
						"The value of the variable.",
						NO_NEW_LINE
				)
		);
		COMMAND_ADD("SET_ENV_SHELL", Command_SetEnvShell.class, "Sets a environment variable with the given key to the evaluation of the given shell command.",
				new Argument(
						"key",
						true,
						false,
						"The environment variable to set.",
						NO_WHITE_SPACE
				),
				new Argument(
						"shell",
						true,
						false,
						"The shell command to execute. The stdout will be used as value. Execution waits for the shell command to finish. A non-zero exit code causes the Jake process to terminate.",
						NO_FILTER
				)
		);
		COMMAND_ADD("UNSET_ENV", Command_UnsetEnv.class, "Removes the environment variable with the given key.",
				new Argument(
						"key",
						true,
						false,
						"The environment variable to remove.",
						NO_WHITE_SPACE
				)
		);
		COMMAND_ADD("IS_ENV", Command_IsEnv.class, "Checks if the given environemnt variable exists and/or has the given value.",
				new Argument(
						"key",
						true,
						false,
						"The environment variable to check.",
						NO_WHITE_SPACE
				),
				new Argument(
						"value",
						false,
						true,
						"The value to check the environment variable against.",
						NO_NEW_LINE
				)
		);
		COMMAND_ADD("SECTION", Command_Section.class, "Defines a new section with the given name.",
				new Argument(
						"sectionname",
						true,
						false,
						"The name of the section.",
						NO_WHITE_SPACE
				)
		);
		COMMAND_ADD("END", Command_EndBranch.class, "Ends the current section.");
		COMMAND_ADD("REPLACE", Command_Replace.class, "Replaces the given string with the given replacement in all tracked files.",
				new Argument(
						"search",
						true,
						true,
						"The string to replace.",
						NO_NEW_LINE
				),
				new Argument(
						"replace",
						true,
						true,
						"The string to replace with. This may not contain the search string.",
						NO_NEW_LINE
				)
		);
	}

	public final static String HELP() {
		StringBuilder sb = new StringBuilder();
		LinkedList<String> keys = new LinkedList(STRING_TO_COMMAND.keySet());
		Collections.sort(keys);
		for (String key : keys) {
			sb.append(key);
			Argument[] arguments = COMMAND_ARGUMENTS.get(STRING_TO_COMMAND.get(key));
			for (Argument arg : arguments) {
				sb.append(" ");
				if (arg.Required) {
					sb.append("<").append(arg.Name).append(">");
				} else {
					sb.append("[").append(arg.Name).append("]");
				}
			}
			sb.append("\n  -> ");
			sb.append(STRING_TO_DESCRIPTION.get(key));
			sb.append("\n");
			for (Argument arg : arguments) {
				sb.append("* ");
				if (arg.Required) {
					sb.append("<").append(arg.Name).append(">");
				} else {
					sb.append("[").append(arg.Name).append("]");
				}
				sb.append(" - ").append(arg.Description).append("\n");
			}
			sb.append("\n");
		}
		return (sb.toString());
	}
}
