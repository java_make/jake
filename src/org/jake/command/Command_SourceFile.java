package org.jake.command;

import java.io.File;
import org.jake.Jakefolder;
import org.jake.Logger;
import org.jake.Utility;

public class Command_SourceFile extends Command {

	public Command_SourceFile(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		File sourceFile = Utility.PARSE_USER_PATH(commandArgs_[0]);
		String canonicalPath = Utility.GET_CANONICAL_PATH(LineNumber, sourceFile);

		Utility.ASSET_FILE(LineNumber, sourceFile, canonicalPath);

		if (Jakefolder.SOURCE_FILE_CHANGED(canonicalPath)) {
			Logger.LOG(LineNumber, "Tracking source file '" + Utility.PATH_SANITIZE(sourceFile.getPath()) + "'.", Logger.SEVERITY.DEBUG);
			Jakefolder.SOURCE_FILE_ADD(LineNumber, canonicalPath);
		} else {
			Logger.LOG(LineNumber, "Source file '" + canonicalPath + "' does not need to be re-added.", Logger.SEVERITY.DEBUG);
		}
	}
}
