package org.jake.command;

import java.io.File;
import org.jake.Jakefolder;
import org.jake.Logger;
import org.jake.Utility;

public class Command_LibraryFile extends Command {

	protected boolean libraryFileAdded = false;
	protected boolean warnNotJarFile = true;

	public Command_LibraryFile(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		libraryFileAdded = false;

		File sourceFile = Utility.PARSE_USER_PATH(commandArgs_[0]);
		String canonicalPath = Utility.GET_CANONICAL_PATH(LineNumber, sourceFile);

		Utility.ASSET_FILE(LineNumber, sourceFile, canonicalPath);

		if (!sourceFile.getName().toLowerCase().endsWith(".jar")) {
			Logger.LOG(LineNumber, "Library file '" + canonicalPath + "' is not a jar file.", warnNotJarFile ? Logger.SEVERITY.WARN : Logger.SEVERITY.DEBUG);
			return;
		}

		libraryFileAdded = true;

		if (Jakefolder.LIBRARY_FILE_CHANGED(canonicalPath)) {
			Logger.LOG(LineNumber, "Tracking library file '" + Utility.PATH_SANITIZE(sourceFile.getPath()) + "'.", Logger.SEVERITY.DEBUG);
			Jakefolder.LIBRARY_FILE_ADD(LineNumber, canonicalPath);
		} else {
			Logger.LOG(LineNumber, "Library file '" + canonicalPath + "' does not need to be re-added.", Logger.SEVERITY.DEBUG);
		}
	}
}
