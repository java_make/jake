package org.jake.command;

import java.io.File;
import java.util.List;
import org.jake.Jakefolder;
import org.jake.Logger;
import org.jake.Utility;

public final class Command_DistributeLibraries extends Command {

	public Command_DistributeLibraries(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		List<File> libraryFiles = Utility.LIST_FILES_RECURSIVE(Jakefolder.LIBRARY_FOLDER);

		if (libraryFiles.isEmpty()) {
			Logger.LOG(LineNumber, "No library files to be distributed.", Logger.SEVERITY.WARN);
			return;
		}

		File distLibs = Jakefolder.FOLDER_CREATE("dist/lib");
		StringBuilder manifest = new StringBuilder();
		manifest.append("Class-Path:");

		for (File sourceFile : libraryFiles) {
			Utility.COPY_OR_REPLACE_FILE(LineNumber, sourceFile, new File(distLibs, sourceFile.getName()));
			manifest.append(" lib/").append(sourceFile.getName());
		}

		new Command_Manifest(LineNumber, Utility.SPACES_ESCAPE(manifest.toString())).execute();
		Logger.LOG(LineNumber, "Distributed " + libraryFiles.size() + " library file(s).", Logger.SEVERITY.INFO);
	}

}
