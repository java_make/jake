package org.jake.command;

import java.io.File;
import java.util.List;
import org.jake.Logger;
import org.jake.Utility;

public final class Command_LibraryFolder extends Command_LibraryFile {

	public Command_LibraryFolder(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
		warnNotJarFile = false;
	}

	@Override
	public void execute() {
		File sourceFile = Utility.PARSE_USER_PATH(commandArgs_[0]);
		String canonicalPath = Utility.GET_CANONICAL_PATH(LineNumber, sourceFile);

		Logger.LOG(LineNumber, "Tracking library folder '" + Utility.PATH_SANITIZE(sourceFile.getPath()) + "'.", Logger.SEVERITY.INFO);

		Utility.ASSET_FOLDER(LineNumber, sourceFile, canonicalPath);

		int numberOfFiles = 0;
		List<File> files = Utility.LIST_FILES_RECURSIVE(sourceFile);
		for (File f : files) {
			commandArgs_[0] = Utility.PATH_SANITIZE(f.getPath());
			super.execute();
			if (libraryFileAdded) {
				numberOfFiles++;
			}
		}

		Logger.LOG(LineNumber, "Tracked " + numberOfFiles + " library files in folder '" + Utility.PATH_SANITIZE(sourceFile.getPath()) + "'.", Logger.SEVERITY.INFO);
	}
}
