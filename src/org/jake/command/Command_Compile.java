package org.jake.command;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;
import org.jake.Jakefolder;
import org.jake.Logger;
import org.jake.Utility;

public final class Command_Compile extends Command {

	public Command_Compile(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		List<File> sourceFiles = Utility.LIST_FILES_RECURSIVE(Jakefolder.SOURCE_FOLDER);
		List<File> sourceTreeFiles = Utility.LIST_FILES_RECURSIVE(Jakefolder.SOURCE_TREE_FOLDER);

		LinkedList<String> javaSourceFiles = new LinkedList<>();
		LinkedList<File> resourceFiles = new LinkedList<>();

		for (File f : sourceFiles) {
			String canonicalPath = Utility.GET_CANONICAL_PATH(LineNumber, f);

			if (!f.getName().endsWith(".java")) {
				continue;
			}

			if (!Jakefolder.BUILD_FILE_CHANGED(canonicalPath)) {
				Logger.LOG(LineNumber, "Source file '" + canonicalPath + "' does not need to be recompiled.", Logger.SEVERITY.DEBUG);
				continue;
			}

			javaSourceFiles.add(canonicalPath);
		}

		for (File f : sourceTreeFiles) {
			String canonicalPath = Utility.GET_CANONICAL_PATH(LineNumber, f);

			boolean isJavaFile = f.getName().endsWith(".java");

			if (!Jakefolder.BUILD_FILE_CHANGED(canonicalPath)) {
				if (isJavaFile) {
					Logger.LOG(LineNumber, "Source file '" + canonicalPath + "' does not need to be recompiled.", Logger.SEVERITY.DEBUG);
				} else {
					Logger.LOG(LineNumber, "Resource file '" + canonicalPath + "' does not need to be re-added.", Logger.SEVERITY.DEBUG);
				}
				continue;
			}

			if (isJavaFile) {
				Jakefolder.PREPARE_REBUILD_TREE_FILE(LineNumber, canonicalPath);
				javaSourceFiles.add(canonicalPath);
			} else {
				resourceFiles.add(f);
			}
		}

		if (!javaSourceFiles.isEmpty()) {

			Logger.LOG(LineNumber, "Compiling " + javaSourceFiles.size() + " Java source file(s).", Logger.SEVERITY.INFO);

			File temp;
			try {
				temp = Files.createTempFile("jake_" + System.currentTimeMillis(), null).toFile();
				try (FileWriter fr = new FileWriter(temp)) {
					for (String sourceFile : javaSourceFiles) {
						fr.write(sourceFile + "\n");
						fr.flush();
					}
				}
				temp.deleteOnExit();
			} catch (IOException ex) {
				Logger.CRASH(LineNumber, ex);
				return;
			}

			String projectBuildPath = Utility.GET_CANONICAL_PATH(LineNumber, Jakefolder.BUILD_FOLDER);

			new Command_Process(LineNumber, new String[]{
				"javac",
				"-d",
				projectBuildPath,
				"-cp",
				Jakefolder.CLASSPATH_GET(projectBuildPath),
				"@" + Utility.GET_CANONICAL_PATH(LineNumber, temp)
			}).execute();
		} else {
			Logger.LOG(LineNumber, "No Java source files need to be (re)compiled.", Logger.SEVERITY.WARN);
		}

		for (File f : sourceFiles) {
			String canonicalPath = Utility.GET_CANONICAL_PATH(LineNumber, f);
			Jakefolder.BUILD_FILE_ADDED(LineNumber, f, canonicalPath, false);
		}

		for (File f : sourceTreeFiles) {
			String canonicalPath = Utility.GET_CANONICAL_PATH(LineNumber, f);
			Jakefolder.BUILD_FILE_ADDED(LineNumber, f, canonicalPath, true);
		}

		Jakefolder.SAVE_TRACKED_FILES();

		if (!resourceFiles.isEmpty()) {
			Logger.LOG(LineNumber, "Adding " + resourceFiles.size() + " resource file(s).", Logger.SEVERITY.INFO);
			for (File sourceFile : resourceFiles) {
				String relativePath = Utility.GET_RELATIVE_PATH(LineNumber, Jakefolder.SOURCE_TREE_FOLDER, sourceFile.getParentFile());

				File destFolder = new File(Jakefolder.BUILD_FOLDER, relativePath);
				File destFile = new File(destFolder, sourceFile.getName());

				destFolder.mkdirs();
				Utility.COPY_OR_REPLACE_FILE(LineNumber, sourceFile, destFile);
			}
		}
	}
}
