package org.jake.command;

import org.jake.Environment;

public class Command_IsEnv extends Command_Branch {

	public Command_IsEnv(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		if (commandArgs_.length == 1) {
			ConditionMet = Environment.ENVIRONMENT_VARS.containsKey(commandArgs_[0]);
		} else if (commandArgs_.length == 2) {
			ConditionMet = Environment.ENVIRONMENT_VARS.containsKey(commandArgs_[0])
					&& Environment.ENVIRONMENT_VARS.get(commandArgs_[0]).equals(commandArgs_[1]);
		}
	}

}
