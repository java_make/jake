package org.jake.command;

import org.jake.Environment;
import org.jake.Logger;
import org.jake.Utility;

public final class Command_Shell extends Command_Process {

	private final String shellCommand_;

	Command_Shell(int _lineNumber, String _commandArgs, boolean _saveOutput) {
		super(_lineNumber, _commandArgs, _saveOutput);
		shellCommand_ = _commandArgs;
	}

	public Command_Shell(int _lineNumber, String _commandArgs) {
		this(_lineNumber, _commandArgs, false);
	}

	@Override
	public void execute() {
		Logger.LOG(LineNumber, "$ " + shellCommand_ + "", Logger.SEVERITY.INFO);
		switch (Utility.GET_OS()) {
			case WINDOWS:
				commandArgs_ = new String[]{
					"cmd.exe",
					"/c",
					shellCommand_
				};
				break;
			default:
				String shell = Environment.ENVIRONMENT_VARS.get("SHELL");
				if (shell == null || shell.isEmpty()) {
					shell = "bash";
				}

				commandArgs_ = new String[]{
					shell,
					"-c",
					shellCommand_
				};
				break;
		}
		super.execute();
	}

}
