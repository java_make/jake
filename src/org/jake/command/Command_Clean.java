package org.jake.command;

import org.jake.Jakefolder;

public final class Command_Clean extends Command {

	public Command_Clean(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		Jakefolder.CLEAN(LineNumber, commandArgs_[0]);
	}

}
