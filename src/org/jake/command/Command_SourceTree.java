package org.jake.command;

import java.io.File;
import java.util.List;
import org.jake.Jakefolder;
import org.jake.Logger;
import org.jake.Utility;

public final class Command_SourceTree extends Command {

	public Command_SourceTree(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		File sourceFile = Utility.PARSE_USER_PATH(commandArgs_[0]);
		String canonicalPath = Utility.GET_CANONICAL_PATH(LineNumber, sourceFile);

		Logger.LOG(LineNumber, "Tracking source tree '" + Utility.PATH_SANITIZE(sourceFile.getPath()) + "'.", Logger.SEVERITY.INFO);

		Utility.ASSET_FOLDER(LineNumber, sourceFile, canonicalPath);

		List<File> files = Utility.LIST_FILES_RECURSIVE(sourceFile);
		for (File f : files) {
			String relativePath, subCanonicalPath;

			relativePath = Utility.GET_RELATIVE_PATH(LineNumber, sourceFile, f.getParentFile());
			subCanonicalPath = Utility.GET_CANONICAL_PATH(LineNumber, f);

			if (Jakefolder.SOURCE_FILE_CHANGED(subCanonicalPath)) {
				Logger.LOG(LineNumber, "Tracking '" + (relativePath + "/" + f.getName()) + "'", Logger.SEVERITY.DEBUG);
				Jakefolder.SOURCE_TREE_FILE_ADD(LineNumber, f, relativePath);
			} else {
				Logger.LOG(LineNumber, "Source tree file '" + (relativePath + "/" + f.getName()) + "' does not need to be re-added.", Logger.SEVERITY.DEBUG);
			}
		}

		Logger.LOG(LineNumber, "Tracked " + files.size() + " source tree files in '" + Utility.PATH_SANITIZE(sourceFile.getPath()) + "'.", Logger.SEVERITY.INFO);
	}
}
