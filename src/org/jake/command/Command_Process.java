package org.jake.command;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import org.jake.Environment;
import org.jake.Logger;
import org.jake.Main;

public class Command_Process extends Command {

	private final boolean saveOutput_;
	protected String Output = null;

	Command_Process(int _lineNumber, String[] _args) {
		super(_lineNumber);
		commandArgs_ = _args;
		saveOutput_ = false;
	}

	Command_Process(int _lineNumber, String _commandArgs, boolean _saveOutput) {
		super(_lineNumber);
		saveOutput_ = _saveOutput;
	}

	public Command_Process(int _lineNumber, String _commandArgs) {
		this(_lineNumber, _commandArgs, false);
	}

	@Override
	public void execute() {
		Logger.LOG(LineNumber, "Execute '" + String.join(" ", commandArgs_) + "'.", Logger.SEVERITY.DEBUG);

		ProcessBuilder processBuilder = new ProcessBuilder(commandArgs_);
		processBuilder.directory(new File(Main.WORKING_DIRECTORY));

		if (!saveOutput_) {
			processBuilder.inheritIO();
		}

		Map<String, String> environment = processBuilder.environment();
		Environment.UPDATE(environment);

		Process process;
		try {
			process = processBuilder.start();
		} catch (Exception ex) {
			Logger.CRASH(LineNumber, ex);
			return;
		}

		try {
			int exitCode = process.waitFor();
			if (exitCode != 0) {
				Logger.CRASH(LineNumber, "Process exited with non-zero exit code '" + exitCode + "'.");
				return;
			}
		} catch (InterruptedException ex) {
			Logger.CRASH(LineNumber, ex);
			return;
		}

		if (saveOutput_) {
			InputStream is = process.getInputStream();
			byte[] buffer = new byte[32];
			int readBytes;
			StringBuilder output = new StringBuilder();
			try {
				while ((readBytes = is.read(buffer)) != -1) {
					output.append(new String(buffer, 0, readBytes));
				}
			} catch (IOException ex) {
				Logger.CRASH(LineNumber, ex);
				return;
			}
			Output = output.toString();
		}
	}
}
