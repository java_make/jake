package org.jake.command;

import java.util.LinkedList;
import org.jake.Environment;
import org.jake.Logger;
import org.jake.command.Commands.Argument;

public abstract class Command {

	public final int LineNumber;
	private String arguments_;
	protected String[] commandArgs_;

	Command(int _lineNumber) {
		LineNumber = _lineNumber;
	}

	Command(int _lineNumber, String _commandArgs) {
		this(_lineNumber);

		while (!_commandArgs.isEmpty() && Character.isWhitespace(_commandArgs.charAt(0))) {
			_commandArgs = _commandArgs.substring(1);
		}

		arguments_ = _commandArgs;
		commandArgs_ = arguments_.split("(?<!\\\\) ");

		for (int i = 0; i < commandArgs_.length; i++) {
			commandArgs_[i] = commandArgs_[i].replaceAll("\\\\ ", " ");
		}

		Argument[] argumentSpecification = Commands.COMMAND_ARGUMENTS.get(getClass());
		if (argumentSpecification == null) {
			return;
		}

		int minArgs = 0;
		int maxArgs = 0;

		for (Argument a : argumentSpecification) {
			if (a.Required) {
				minArgs++;
			}
			maxArgs++;
		}

		if (commandArgs_.length == 1 && commandArgs_[0].isEmpty()) {
			commandArgs_ = new String[0];
		}

		if (commandArgs_.length < minArgs) {
			Logger.CRASH(LineNumber, "Missing argument <" + argumentSpecification[commandArgs_.length].Name + ">.");
			return;
		}

		if (commandArgs_.length > maxArgs) {
			Logger.CRASH(LineNumber, "Invalid number of arguments.");
			return;
		}

		for (int i = 0; i < commandArgs_.length && i < argumentSpecification.length; i++) {
			String arg = commandArgs_[i];
			Argument argSpec = argumentSpecification[i];
			if (!argSpec.Filter.test(arg)) {
				Logger.CRASH(LineNumber, "Invalid argument for <" + argSpec.Name + "> = '" + arg + "'.");
				return;
			}
		}
	}

	public final void prepareAndExecute() {
		Argument[] argumentSpecification = Commands.COMMAND_ARGUMENTS.get(getClass());
		LinkedList<String> sortedVariables = null;

		if (commandArgs_ != null && argumentSpecification != null) {
			for (int i = 0; i < commandArgs_.length && i < argumentSpecification.length; i++) {
				if (!argumentSpecification[i].ResolveEnvironment) {
					continue;
				}

				String argument = commandArgs_[i];
				if (!argument.contains("$")) {
					continue;
				}

				if (sortedVariables == null) {
					sortedVariables = new LinkedList<>(Environment.ENVIRONMENT_VARS.keySet());
					sortedVariables.sort((String s1, String s2) -> {
						return (s2.length() - s1.length());
					});
				}

				for (String key : sortedVariables) {
					String env = "$" + key;
					if (argument.contains(env)) {
						argument = argument.replaceAll("\\" + env, Environment.ENVIRONMENT_VARS.get(key));
					}
				}

				// Re-Check, if the argument is _still_ valid
				Argument argSpec = argumentSpecification[i];
				if (!argSpec.Filter.test(argument)) {
					Logger.CRASH(LineNumber, "Invalid resolved argument for <" + argSpec.Name + "> = '" + argument + "'.");
					return;
				}

				commandArgs_[i] = argument;
			}
		}

		execute();
	}

	protected abstract void execute();

	@Override
	public String toString() {
		return (getClass().getSimpleName() + ((commandArgs_ != null && commandArgs_.length != 0) ? ": '" + String.join(" ", commandArgs_) + "'" : ""));
	}

}
