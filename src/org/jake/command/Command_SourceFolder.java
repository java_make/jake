package org.jake.command;

import java.io.File;
import java.util.List;
import org.jake.Logger;
import org.jake.Utility;

public final class Command_SourceFolder extends Command_SourceFile {

	public Command_SourceFolder(int _lineNumber, String _commandArgs) {
		super(_lineNumber, _commandArgs);
	}

	@Override
	public void execute() {
		File sourceFile = Utility.PARSE_USER_PATH(commandArgs_[0]);
		String canonicalPath = Utility.GET_CANONICAL_PATH(LineNumber, sourceFile);

		Utility.ASSET_FOLDER(LineNumber, sourceFile, canonicalPath);

		List<File> files = Utility.LIST_FILES_RECURSIVE(sourceFile);
		for (File f : files) {
			commandArgs_[0] = Utility.PATH_SANITIZE(f.getPath());
			super.execute();
		}

		Logger.LOG(LineNumber, "Tracked " + files.size() + " source files in folder '" + Utility.PATH_SANITIZE(sourceFile.getPath()) + "'.", Logger.SEVERITY.INFO);
	}
}
