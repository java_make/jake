package org.jake;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import org.jake.command.Command;
import org.jake.command.Commands;

public final class JakefileParser {

	private static int LINE_NUMBER = 0;

	public final static Jakefile PARSE_FILE(File _file) {
		LINE_NUMBER = 0;

		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(_file));
			Logger.LOG(LINE_NUMBER, "Parsing jakefile at '" + Utility.GET_CANONICAL_PATH(LINE_NUMBER, _file) + "'.", Logger.SEVERITY.DEBUG);
		} catch (FileNotFoundException ex) {
			Logger.CRASH(LINE_NUMBER, ex);
			return (null);
		}

		LinkedList<Command> commands = new LinkedList<>();
		while (true) {
			LINE_NUMBER++;
			String line;

			try {
				line = br.readLine();
			} catch (IOException ex) {
				Logger.CRASH(LINE_NUMBER, ex);
				return (null);
			}

			if (line == null) {
				break;
			}

			Command c = PARSE_LINE(line);
			if (c != null) {
				commands.add(c);
			}
		}

		try {
			br.close();
		} catch (IOException ex) {
			Logger.CRASH(LINE_NUMBER, ex);
			return (null);
		}

		return (new Jakefile(commands));
	}

	private static Command PARSE_LINE(String _line) {
		while (!_line.isEmpty() && Character.isWhitespace(_line.charAt(0))) {
			_line = _line.substring(1);
		}

		if (_line.isEmpty()) {
			return (null);
		}

		if (_line.startsWith("#")) {
			return (null);
		}

		if (_line.startsWith("//")) {
			return (null);
		}

		if (_line.startsWith("--")) {
			return (null);
		}

		String[] commandFull = _line.split(" ", 2);
		String commandName = commandFull[0];
		Class<? extends Command> commandClass = Commands.STRING_TO_COMMAND.get(commandName);

		if (commandClass == null) {
			Logger.CRASH(LINE_NUMBER, "Unknown command '" + commandName + "'.");
			return (null);
		}

		try {
			return (commandClass.getConstructor(int.class, String.class).newInstance(LINE_NUMBER, commandFull.length > 1 ? commandFull[1] : ""));
		} catch (Exception ex) {
			Logger.CRASH(LINE_NUMBER, ex);
			return (null);
		}
	}
}
