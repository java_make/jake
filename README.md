# Jake
``jake`` is a lightweight minimal build system written in Java for Java.

[![latest binary](https://gitlab.com/java_make/jake/-/badges/release.svg)](https://gitlab.com/java_make/jake/-/releases) [![pipeline status](https://gitlab.com/java_make/jake/badges/master/pipeline.svg)](https://gitlab.com/java_make/jake/-/commits/master)  

### Installation

Download the [latest binary](https://gitlab.com/java_make/jake/-/releases) - It is portable.  
Optionally, add a script to your ``$PATH`` which calls ``java -jar jake.jar $@`` and call it ``jake``.

### Usage

Start the ``jake`` binary in a working directory which contains a ``Jakefile``.  
Optionally, provide additional arguments and sections.

The ``jake`` binary searches for a file called ``Jakefile`` (any case permutation) and works through it line by line.  
Every line may contain one of a set of predefined commands, a comment (denoted by ``//``, ``#``, or ``--``), or nothing.  
Whitespace at the beginning of each line is ignored.

``jake`` creates a folder in the current directory called ``.jake`` where it stores a copy of its working files.  
The compiled ``.jar`` files will be located at ``.jake/dist``.

### License

Public domain code. For more information see [Unlicense](/LICENSE).

### Build from source

Requirements: git, bash, Java JDK (javac, jar), Java base-JRE, Jake

* Download or clone this repository
* Place any ``jake`` binary in the repository root directory, or add it to your ``$PATH``
* Open the repository root directory in a terminal of your choice
* Run ``jake``
* The compiled ``jake`` binary will be placed in ``.jake/dist/jake.jar``

### Why?

* I do not want to fiddle with ``javac``, ``jar`` and ``manifest.mf`` files by hand
* I do not need a package manager for my personal Java projects
* I do not want to read more than 50 lines of documentation to build a ``Hello World!`` Java program
* I do not like XML
* Just give me a CLI tool equivalent to the "Build" button in any IDE!
* Also, why not? Learning is fun!

### Example Jakefiles

#### Minimal example
Jakefile:
```
SOURCE_TREE src
COMPILE
JAR Application.jar org.Main
```

#### Execute shell commands from Jake
Jakefile:
```
SOURCE_TREE src
COMPILE
JAR Application.jar org.Main
$ echo Hello World!
```

#### Sections
Sections may be defined using the ``SECTION`` or the ``IS_ENV`` comand.  
These are only executed, if the section was provided as a command line argument.  
Sections closed with the ``END`` command.
Jakefile:
```
// Executed on every run

SECTION Install
    // Only executed, if Jake was started with "jake Install"
END

// Executed on every run
```

### List of Jake commands
| Command | Arguments | Description |
|---|---|---|
| ``SOURCE_TREE`` | ``tree`` | Adds a source tree. This will all sub-folders and files recursively for compilation. Resource files, aka non ``.java`` files, will also be included. |
| ``SOURCE_FILE`` | ``file`` | Adds a source file. This will track an unorganized java file for compilation. No resource files can be added using ``SOURCE_FILE``. The argument has to be a ``.java`` file. |
| ``SOURCE_FOLDER`` | ``folder`` | Adds a source folder. This will track all unorganized java files in the folder recursively for compilation. Non ``.java`` files are ignored. |
| ``LIBRARY_FOLDER`` | ``folder`` | Adds all ``.jar`` files in the ``folder`` recursively to the list of compile time libraries. Non ``.jar`` files are ignored. |
| ``LIBRARY_FILE`` | ``file`` | Adds a ``.jar`` file to the list of compile time libraries. Non ``.jar`` files are ignored. |
| ``REPLACE`` | ``search``, ``replace`` | Replaces the given ``search`` string with the provided ``replace`` string in all tracked source files. The replace string may not contain the search string. |
| ``COMPILE`` | N/A | Compiles all currently tracked source files |
| ``DISTRIBUTE_LIBRARIES`` | N/A | Adds the tracked library files to the ``dist`` folder and adds them to the ``.jar`` file classpath. Call this before ``JAR``. |
| ``MANIFEST`` | ``content`` | Adds the given string to the MANIFEST.MF file, which is later included in the ``.jar`` file. The argument may not contain newline characters. |
| ``JAR`` | ``filename``, ``main-class`` | Creates the ``.jar`` archive with the given ``filename``. Optionally, specify the main-class the ``.jar`` file should execute. |
| ``CLEAN`` | ``folder`` | Cleans the specified internal Jake folder or file. Possible arguments are ``SOURCES``, ``LIBRARIES``, ``BUILD``, ``DIST`` and ``MANIFEST``. |
| ``$`` | ``shell`` | Executes ``shell`` in the host system shell. |
| ``SET_ENV`` | ``key``, ``value`` | Sets the given environment variable to the given value. |
| ``SET_ENV_SHELL`` | ``key``, ``shell`` | Sets the given environment variable the result of executing the second argument as a shell command (stdout). |
| ``UNSET_ENV`` | ``key`` | Removes the given environment variable. |
| ``IS_ENV`` | ``key``, ``value`` | Checks, if the given environment variable is set. If the second argument is provided, checks also the content. End this section using ``END``. |
| ``SECTION`` | ``section`` | Checks, if the given section was provided as command line parameter. End this section using ``END``. |
| ``END`` | N/A | End the current section. |
